from bs4 import BeautifulSoup as bso
import requests

##### TODO: GET DAILY MENU MORE CLEARLY

class EatEtc:
    def __init__(self):
        self.name = "EatEtc Crawler"
        self.crawler_eatetc()

    def crawler_eatetc(self):
        url = "http://meniu.eat-etc.ro/"

        headers = {
            'User-Agent': "PostmanRuntime/7.15.2",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Postman-Token': "27a2180d-a2a5-4575-ba56-08921d0a3955,d04ce64a-bf83-4f65-8cfe-965fd0ec9dc6",
            'Host': "meniu.eat-etc.ro",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", url, headers=headers)
        soup = bso(response.text, "html.parser")
        for index in range(6, len(soup.find_all("span"))):
            print(soup.find_all("span")[index].get_text())
eatetc = EatEtc()
