from bs4 import BeautifulSoup as bso
import requests
import unidecode
import re

##### TODO: GET DAILY MENU MORE CLEARLY

class Occo:
    def __init__(self):
        self.name = "Occo Crawler"
        self.crawler_occo()

    def crawler_occo(self):
        url = "https://www.occo.ro/"

        headers = {
            'Content-type': 'text/plain; charset=utf-8',
            'User-Agent': "PostmanRuntime/7.16.3",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Postman-Token': "c59bac49-9563-4f35-b957-9fdda7e98acc,055dd5cd-66d4-4806-9375-35e3395f3741",
            'Host': "www.occo.ro",
            'Accept-Encoding': "gzip, deflate",
            'Cookie': "__cfduid=d2681fff5c31fb099a0ebe3632d4446671567681865",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", url, headers=headers)
        response.encoding = 'utf-8'
        soup = bso(response.text, "html.parser")
        for index in range(0, len(soup.find_all("p"))-3):
            #food = re.sub( r'(\))', r'\g<1> \n', )
            print(soup.find_all("p")[index].get_text())
            food = re.sub(r'(\s)(\d)([A-Z])', r'\1\2\n\3', soup.find_all("p")[index].get_text())
            food = re.sub(r'(\s)(\d\d)([A-Z])', r'\1\n\3', food)
            food = re.sub(r'(\-)(\s)(\d\d)(\s)', r'\1\2\3\n', food)
            food = re.sub(r'(\-)(\s)(\d)(\s)', r'\1\2\3\n', food)
            #food = re.sub(r'(ml\))', r'\1\n', food)
            #food = re.sub(r'(gr\))', r'\1\n', food)
            #food = re.sub(r'(\w)(\))(\w&^\s)', r'\1\2\n\3', food)
            #food = re.sub(r'(\d)([ ])', r'\1\n\2', food)
            #print(soup.find_all("p")[index].get_text())
            food.strip()
           # print(food)
occo = Occo()

#### TODO: MODIFY REGEX TO PRINTOUT CORRECTLY