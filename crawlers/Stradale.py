import urllib.request as urllib2
from bs4 import BeautifulSoup as bso


class Stradale:
    def __init__(self):
        self.name = "Stradale Crawler"
        self.crawler_stradale()

    def switch_demo(self, argument):
        switcher = {
            2: "Cafele",
            3: "Mic Dejun",
            4: "Supe",
            5: "Salate",
            6: "Curries & Stews",
            7: "Garnituri",
            8: "Fried & Fries",
            9: "Robata & Tandoor",
            10: "Desert",
            11: "Naan Bread",
            12: "Meniul zilei 23 lei",
            13: "Meniul zilei vegetarian 16 lei"
        }
        print (switcher.get(argument, "Invalid month"))

    def crawler_stradale(self):
        url = 'http://stradale.ro/locatie/stradale-amazon/'

        response = urllib2.urlopen(url)
        webContent = response.read()
        soup = bso(webContent,"lxml")

        for index in range(2, len(soup.find_all("p")) - 4):
            self.switch_demo(index)
            print(soup.find_all("p")[index].get_text())
            print("\n")

stradale = Stradale()


